#!/bin/sh
################################################################################
# This script will setup a new CentOS 7 machine with the following:
#
# * new root password
# * new user with sudo ability
# * firewall enabled
# * SSH root login disabled
# * SSH port changed
# * SSH keys generated for new user
#
# Usage:
#   ./setupCentOS7.sh <new user name>
#
# Example:
#   ./setupCentOS7.sh john
#
# Keys and passwords will be output to files and zipped into a file named
# setup_logs.zip.
################################################################################

################################################################################
###                                                                          ###
###                                 Variables                                ###
###                                                                          ###
################################################################################

# No need to modify these
new_user=$1
new_user_pass=""
root_pass=""
key_pass=""
ssh_port=""
root_home="/root"

# May need to modify these
ssh_conf="/etc/ssh/sshd_config"
key_file="${new_user}_key"
log="passwords.txt"
tar_file="setup_logs.tar"
fin_script="finish.sh"

################################################################################
###                                                                          ###
###                                 Functions                                ###
###                                                                          ###
################################################################################

################################################################################
#   gen_random_pass [len]
#   len The length of the password to be generated (default is 16)
#
#   Generates a new password of the given length.
#
#   Inspired by:
#   http://www.cyberciti.biz/faq/linux-unix-generating-passwords-command/
################################################################################
gen_random_pass()
{
    local len=$1
    if [ "$len" == "" ]; then
        len=16
    fi
    mkpasswd -l $len
}

################################################################################
#   gen_new_root_pass
#
#   Generates a new password for the root user
################################################################################
gen_new_root_pass()
{
    new_root_pass=`gen_random_pass 16`
}

################################################################################
#   gen_new_user_pass
#
#   Generates a new password for the user
################################################################################
gen_new_user_pass()
{
    new_user_pass=`gen_random_pass 16`
}

################################################################################
#   gen_key_pass
#
#   Generates a new password for the user's SSH keys
################################################################################
gen_key_pass()
{
    key_pass=`gen_random_pass 16`
}

################################################################################
#   gen_ssh_port
#
#   Generates a new port for SSH in the range 1025 - 65534. Not 65535 due to the
#   limitations of $RANDOM.
################################################################################
gen_ssh_port()
{
    base=1024
    top=65535
    num1=$RANDOM
    num2=$RANDOM
    sum=$((num1 + num2))
    sum=$((sum % (top - base)))
    ssh_port=$((sum + base))
}

################################################################################
###                                                                          ###
###                                 Script                                   ###
###                                                                          ###
################################################################################

if [[ -z "${new_user// }" ]]; then
    echo "Please provide a user name as the only argument"
    exit
fi
gen_new_root_pass
gen_new_user_pass
gen_key_pass
gen_ssh_port

# Change root password
echo $new_root_pass | passwd root --stdin

# Setup new user
useradd $new_user -p $new_user_pass
usermod -a -G wheel $new_user
echo -n -e "\n$new_user\tALL=(ALL)\tNOPASSWD: ALL" >> /etc/sudoers

# Enable the firewall
systemctl enable firewalld
systemctl start firewalld

# Modify SSH
echo "Port $ssh_port" >> $ssh_conf
systemctl restart sshd

firewall-cmd --zone=public --add-port=$ssh_port/tcp --permanent
firewall-cmd --reload
systemctl restart sshd

# Add the SSH port in SELinux
yum install policycoreutils-python -y
semanage port -a -t ssh_port_t -p tcp $ssh_port

# Generate Keys
mkdir /home/$new_user/.ssh
ssh-keygen -t rsa -b 2048 -N $key_pass -f $root_home/$key_file
cat ${key_file}.pub > /home/$new_user/.ssh/authorized_keys
chmod 600 /home/$new_user/.ssh/authorized_keys

echo "Setup Log" > $log

echo "New Root Pass: $new_root_pass" >> $log
echo "New User Name: $new_user" >> $log
echo "New User Pass: $new_user_pass" >> $log
echo "New Key  Pass: $key_pass" >> $log
echo "New SSH  Port: $ssh_port" >> $log

cd $root_home
if [ -e $tar_file ]; then
    rm -f $tar_file
fi
tar -cf $tar_file $log $key_file ${key_file}.pub
rm $log $key_file ${key_file}.pub
cd -

# Generate the final script to disable root login and password authentication
echo "echo \"PermitRootLogin no\" >> $ssh_conf" >> $fin_script
echo "sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/g' $ssh_conf" >> $fin_script
echo "systemctl restart sshd" >> $fin_script
chmod +x $fin_script

echo "Setup completed"
echo "Login as root on port " $ssh_port " with the password: " $new_root_pass
echo "Download $tar_file and test SSH before disconnecting"
echo "Finally, run the finish.sh script to complete the setup"

