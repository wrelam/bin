#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

my $spacesPerTab = 4;

my $name;
my $value;
my $archive;
my $spOnly;
my $gameDLL;
my $cheat;
my $userInfo;
my $notify;
my $protected;
my $printableOnly;
my $unlogged;
my $neverAsString;
my $replicated;
my $demo;
my $dontRecord;
my $serverCanExecute;
my $clientCmdCanExecute;
my $clientDLL;
my $ss;
my $ssAdded;
my $blank;
my $helpText;

my %cmdList;
my $maxNameLen = 0;
my $maxTabs = 0;
my $tabs = 0;
my $numTabs = 0;
my $maxKey = "";

sub
getSpacesToNextTab($)
{
    my $len = length($_[0]);
    my $spacesToNextTab = $len % $spacesPerTab;

    if ($spacesToNextTab != 0)
    {
        $spacesToNextTab = $spacesPerTab - $spacesToNextTab;
    }
    else
    {
        $spacesToNextTab = 4;
    }

    return $spacesToNextTab;
}

sub
getTabs($)
{
    my $len = length($_[0]);
    my $spacesToNextTab = getSpacesToNextTab($_[0]);
    
    my $numTabs = ($len + $spacesToNextTab) / $spacesPerTab;

    return $numTabs;
}

sub
test()
{
    printf "Tabs: _         %d\n", getTabs("");
    printf "Tabs: a         %d\n", getTabs("a");
    printf "Tabs: ab        %d\n", getTabs("ab");
    printf "Tabs: abc       %d\n", getTabs("abc");
    printf "Tabs: abcd      %d\n", getTabs("abcd");
    printf "Tabs: abcde     %d\n", getTabs("abcde");
    printf "Tabs: abcdef    %d\n", getTabs("abcdef");
    printf "Tabs: abcdefg   %d\n", getTabs("abcdefg");
    printf "Tabs: abcdefgh  %d\n", getTabs("abcdefgh");
    printf "Tabs: abcdefghi %d\n", getTabs("abcdefghi");
}

# Read in file, generate hash of all cvars
while (<>)
{
    $_ =~ s/\"|\'//g;
    my @fields = split(',', $_, 22);

    # Skip any line that is garbage
    # Could add this line to a string, then append that to last keys $helpText
    if ($#fields != 21)
    {
        next;
    }

    ($name,             $value,                 $archive,       $spOnly,    $gameDLL, 
     $cheat,            $userInfo,              $notify,        $protected, $printableOnly, 
     $unlogged,         $neverAsString,         $replicated,    $demo,      $dontRecord, 
     $serverCanExecute, $clientCmdCanExecute,   $clientDLL,     $ss,        $ssAdded, 
     $blank,            $helpText) = @fields;

    chop $helpText;

    if ($value eq "cmd")
    {
        $value = "   ";
    }

    $cmdList{$name}[0] = "";
    $cmdList{$name}[1] = "$value";
    $cmdList{$name}[2] = "$helpText";
}

# Get longest key name length
foreach (keys %cmdList)
{
    my $key = $_;
    if (length($key) > $maxNameLen)
    {
        $maxNameLen = length($key);
        $maxKey = $key;
    }
}

$maxTabs = getTabs($maxKey);

# Print name with correct number of tabs
foreach (sort keys %cmdList)
{
    my $key = $_;
    $numTabs = $maxTabs - getTabs($_);
    
    $cmdList{$key}[0] = "$_";
    $cmdList{$key}[0] .= " " for (1 .. getSpacesToNextTab($_));
    for (1 .. ($numTabs))
    {
        $cmdList{$key}[0] .= " " for (1 .. $spacesPerTab);
    }
}

# Print value and description
$maxNameLen = 0;

foreach (keys %cmdList)
{
    my $key = $_;
    if (length($cmdList{$key}[1]) > $maxNameLen)
    {
        $maxNameLen = length($cmdList{$key}[1]);
        $maxKey = $cmdList{$key}[1];
    }
}

$maxTabs = getTabs($maxKey);

foreach (sort keys %cmdList)
{
    my $key = $_;
    $numTabs = $maxTabs - getTabs($cmdList{$key}[1]);

    $cmdList{$key}[0] .= $cmdList{$key}[1];
    $cmdList{$key}[0] .= " " for (1 .. getSpacesToNextTab($cmdList{$key}[1]));
    for (1 .. ($numTabs))
    {
        $cmdList{$key}[0] .= " " for (1 .. $spacesPerTab);
    }

    $cmdList{$key}[0] .= "// " . $cmdList{$key}[2];
}

# Print out everything
foreach (sort keys %cmdList)
{
    print $cmdList{$_}[0] . "\n";
}


print "0123T567T911T345T789T1223T567T931T345T789T123T567T95\n";
