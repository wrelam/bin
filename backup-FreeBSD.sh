#!/bin/sh
startDir=`pwd`

backupLoc="/tmp/backup"
archiveName="hoboPack"

# Put each file you want to save on a separate line
filesToBackup="
/etc/rc.conf
/boot/loader.conf
/etc/adduser.conf
"

echo "Walt's Magic backup script"

# Make your backup folder, go to it
mkdir -p $backupLoc/backup

# Make an unpack script
touch $backupLoc/unpack.sh
chmod +x $backupLoc/unpack.sh

echo "#!/bin/sh" >> $backupLoc/unpack.sh

# Copy your precious files
for file in $filesToBackup
do
	echo "Processing $file"
	cp $file $backupLoc/backup
	echo "cp backup/${file##*/} $file" >> $backupLoc/unpack.sh
done

# Backup this script
cp $startDir/$0 $backupLoc

# Pack it up, ship it out
tar -cvzf $startDir/$archiveName.tgz -C $backupLoc .

# Cleanup
rm -rf $backupLoc

cd $startDir
